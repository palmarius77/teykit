var server  = require('dpd-starter'),
    fs      = require('fs');

var access = fs.createWriteStream('../../LogFiles/Application/node.access.log', { flags: 'a' })
      , error = fs.createWriteStream('../../LogFiles/Application/node.error.log', { flags: 'a' });

process.stdout.pipe(access);
process.stderr.pipe(error);

server.startServer();