# TeykIt #

The TeykIt platform turns your garbage into someone's treasure.



### How do I get set up? ###

You need to have [Node.js](https://nodejs.org) and [MongoDB](https://www.mongodb.com/download-center?jmp=nav#community) installed

In your project folder
```
#!bash

npm install
npm start
```

By default the app will listen at port 2403

You can find more information about the Deployd framework [here](https://github.com/deployd/deployd)