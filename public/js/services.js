(function () {
    'use strict';
    var teykitApp = angular.module('teykitApp');

    // current user service
    teykitApp.factory('productService', function ($http, $routeParams, $rootScope) {

        var getProduct = function () {
            return $http({method: "GET", url: "/products", params: {id: $routeParams.id}}).then(function (result) {
                return result.data;
            });
        };

        var getFeaturedProducts = function () {
            return $http({method: "GET", url: "/products?{\"amount\": {\"$gt\": 9}}"}).then(function (result) {
                console.log(result);
                return result.data;
            });
        };

        var getProducts = function () {
            return $http({method: "GET", url: "/products", params: {amount: 0}}).then(function (result) {
                return result.data;
            });
        };

        var updateProductServ = function () {
            $rootScope.$broadcast('productUpdated');
        };

        return {
            getProduct: getProduct, 
            getProducts: getProducts, 
            updateProductServ: updateProductServ, 
            getFeaturedProducts: getFeaturedProducts
        };
    });
}());
