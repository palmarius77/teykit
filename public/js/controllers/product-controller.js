(function() {
    'use strict';
    var teykitApp = angular.module('teykitApp');

    // controller for product
    teykitApp.controller('ProductController', function($scope, $rootScope, productService) {

        $scope.product = {};

        $scope.$on('productUpdated', function() {
            var servResponse = productService.getProduct();
            servResponse.then(function(result) {
                $scope.product = result;
            });
        });

        productService.getProduct().then(function(result) {
            $scope.product = result;
        });

        $scope.getPrdTxt = "";
        $scope.getPorduct = function(){
            $scope.getPrdTxt = "The product will be available soon.";
            $scope.getPrdTit = "Thank you for your interest.";
        };

    });


}());