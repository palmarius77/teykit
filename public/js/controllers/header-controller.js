(function() {
    'use strict';
    var teykitApp = angular.module('teykitApp');

    teykitApp.controller('HeaderController', function($scope, $rootScope, productService) {

        $scope.loggedin = false;

        function statusChangeCallback(response) {
            if (response.status === 'connected') {
                // Logged into your app and Facebook.
                $scope.loggedin = true;
                $scope.$apply();
                $scope.appLogin();
            } else if (response.status === 'not_authorized') {

            } else {

            }
        }

        window.checkLoginState = function() {
            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });
        };

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1120415151398820',
                cookie     : false,  // enable cookies to allow the server to access
                                    // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v2.5' // use graph api version 2.5
            });

            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });

        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $scope.appLogin = function() {
            FB.api('/me', function(response) {
                $scope.loggedin = true;
                $scope.$apply();
                $scope.fbuser = response;
                var query = {"username":response.id};
                dpd.users.get(query, function (result) {
                    if(!result[0]){
                        // register
                        dpd.users.post({"username": $scope.fbuser.id, "password": $scope.fbuser.id, "name": $scope.fbuser.name}, function(user, err) {
                            if(err) return console.log(err);
                            $scope.user = result[0];
                            //login
                            dpd.users.login({"username": $scope.user.username, "password": $scope.user.username}, function(user, err) {
                                if(err) return console.log(err);
                                $scope.loggedin = true;
                                $scope.$apply();
                                emitLoginEvent();
                            });
                        });
                    }
                    else{
                        //login
                        $scope.user = result[0];
                        dpd.users.login({"username": $scope.user.username, "password": $scope.user.username}, function(user, err) {
                            if(err) return console.log(err);
                            $scope.loggedin = true;
                            $scope.$apply();
                            emitLoginEvent();
                        });
                    }
                });
            });
        };

        $scope.logout = function(){
            FB.logout(function(response) {
                FB.getLoginStatus();
                // user is now logged out
                console.log(response);
                dpd.users.logout(function(err) {
                    if(err) console.log(err);
                    $scope.loggedin = false;
                    $scope.$apply();
                    location.reload();
                });
            });
        };

        function emitLoginEvent() {
            $rootScope.$emit('userLogin');
        }

        $scope.emitLoginEvent = emitLoginEvent;

    });

}());