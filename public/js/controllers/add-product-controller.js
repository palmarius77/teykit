(function() {
    'use strict';
    var teykitApp = angular.module('teykitApp');

    // controller for product
    teykitApp.controller('AddProductController', function($scope, $rootScope, $location) {

        $scope.newProduct = {};
        $scope.files = [];

        $scope.errMessage = '';

        $scope.setForm = function(form) {
            $scope.addProductForm = form;
        };

        $scope.pageAccess = false;
        updatePageAccessPermission();
        $rootScope.$on('userLogin', updatePageAccessPermission);

        function updatePageAccessPermission() {
            dpd.users.me(function(me) {
                if (me) {
                    $scope.pageAccess = true;
                    $scope.$apply();
                }
            });
        }

        $scope.uploadFiles = function() {
            //console.log($scope.files);
            var response;
            var fd = new FormData();
            var fds = [];
            var responses = [];

            // reorder array
            $scope.files.sort(function(a, b) {
                if(a.for < b.for) //sort string ascending
                    return -1;
                if(a.for > b.for)
                    return 1;
                return 0; //default return value (no sorting)
            });
            if($scope.files[0])
                fd.append("uploadedFile", $scope.files[0]);


            var subdir = "products";
            var comments = $scope.newProduct.title;
            var uniqueFilename = true;
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/fileupload?subdir=' + subdir + '&comments=' + comments + '&uniqueFilename=' + uniqueFilename);
            xhr.onload = function() {
                response = JSON.parse(this.responseText);
                if(response.statusCode == 401) {
                    $scope.errMessage = response.message;
                    console.log($scope.errMessage);
                }
                else {
                    $scope.newProduct.image = "/uploads/products/" + response[0].filename;
                    $scope.newProduct.amount = $scope.newProduct.amount ? $scope.newProduct.amount : 0;
                    dpd.products.post($scope.newProduct, function(prod, err) {
                        if(err) {
                            return console.log(err);
                        }
                        $scope.errMessage = "";
                        $scope.newProduct = {};
                        $("#productImage").val("");
                        $scope.$apply();
                        $scope.addProductForm.$setPristine();
                        $location.path('/product/' + prod.id);
                    });
                }
            };
            xhr.onerror = function(err) {
                console.log("Error: ", err);
            };
            xhr.send(fd);

        };

        $scope.setFile = function() {
            var g = 0;
            var actual_files = $("#productImage").prop("files");
            // Turn the FileList object into an Array
            for(var i = 0; i < actual_files.length; i++) {
                var file_ext = actual_files[i].name.split('.');
                file_ext = file_ext[file_ext.length-1].toLowerCase();
                if(file_ext != "jpg" && file_ext != "png" && file_ext != "jpeg") {
                    g = 1;
                    $scope.errMessage = "Wrong extension";
                }
                else if(actual_files[i].size > 3500000) {
                    g = 1;
                    $scope.errMessage = "File too large";
                }
                else {
                    $scope.files = $scope.files.filter(function(item, idx) {
                        return item.for != "a_primary";
                    });
                    $scope.files.push(actual_files[i]);
                    $scope.files[$scope.files.length - 1].for = "a_primary";
                }
            }
            //console.log(actual_files);
            if(g == 0) {
                $scope.errMessage = "";
                $scope.$apply();
            }
            else {
                console.log($scope.errMessage);
                $("#productImage").val("");
                $scope.$apply();
            }
        };

        $scope.cancelImage = function() {
            $("#productImage").val("");
            $scope.errMessage = "";
            $scope.files = $scope.files.filter(function(item, idx) {
                return item.for != "a_primary";
            });
        };

        $scope.addProduct = function() {
            $scope.errMessage = '';
            if($scope.files.length > 0) {
                $scope.uploadFiles();
            }
            else{
                $scope.errMessage = 'Please upload a file';
            }
        };

        $scope.showVoucher = false;

        $scope.showV = function(){
            if($scope.showVoucher == false){
                $scope.showVoucher = true;
            }
        };
    });
}());
