(function() {
    'use strict';
    var teykitApp = angular.module('teykitApp');

    teykitApp.controller('ProductsController', function($scope, products, featured_products) {

        $scope.products = products;
        $scope.featured_products = featured_products;

    });
}());