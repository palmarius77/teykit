(function () {
    'use strict';
    var teykitApp = angular.module('teykitApp');

    // general directives
    teykitApp.directive('headerDirective', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'partials/header.html'
        }
    });
    teykitApp.directive('footerDirective', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'partials/footer.html'
        }
    });

    // custom directive on change function
    teykitApp.directive('customOnChange', function() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var onChangeHandler = scope.$eval(attrs.customOnChange);
                element.bind('change', onChangeHandler);
            }
        };
    });

    /*teykitApp.filter('reverse', function() {
        return function(items) {
            if(items)
                return items.slice().reverse();
        };
    });*/


}());