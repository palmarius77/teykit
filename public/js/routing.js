(function () {
    'use strict';
    var teykitApp = angular.module('teykitApp');

    // config routes
    teykitApp.config(function ($routeProvider /*, $locationProvider*/) {
        //$locationProvider.html5Mode(true);
        $routeProvider
            .when("/", {
                templateUrl: "/partials/view-homepage.html",
                title: "Welcome!",
                controller: 'ProductsController',
                resolve: {
                    products: function (productService) {
                        return productService.getProducts();
                    },
                    featured_products: function (productService) {
                        return productService.getFeaturedProducts();
                    }
                }
            })
            .when("/product/:id", {
                templateUrl: "/partials/view-product.html",
                title: "Product page",
                controller: 'ProductController'
            })
            .when("/add-product", {
                templateUrl: "/partials/view-add-product.html",
                title: "Add product"
            })

            .otherwise({templateUrl: "/partials/view-404.html", title: "Page not found"});
    });

    teykitApp.run(['$rootScope', function ($rootScope) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            $rootScope.title = current.$$route.title;
        });
    }]);

}());